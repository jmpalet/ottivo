<?php
declare(strict_types=1);

namespace App\Application\Calculator\Service;

use App\Application\Calculator\Context\CalculationContext;
use App\Application\Calculator\Rules\EmployedCurrentYearRule;
use App\Application\Calculator\Rules\Rule;
use App\Application\Calculator\Rules\SeniorityRule;
use App\Application\Calculator\Rules\SpecialContractRule;
use App\Domain\Model\Employee;

class VacationCalculator
{
    private const RULE_CLASSES = [
        EmployedCurrentYearRule::class,
        SeniorityRule::class,
        SpecialContractRule::class,
    ];

    /** @var Rule[]  */
    private $rules = [];

    public function __construct()
    {
        foreach (self::RULE_CLASSES as $class) {
            $this->rules[] = new $class;
        }
    }

    public function calculate(CalculationContext $context): int
    {
        foreach ($this->rules as $rule) {
            if ($rule->isApplied($context)) {
                return $rule->calculate($context);
            }
        }

        return Employee::MIN_VACATION_DAYS;
    }

}
