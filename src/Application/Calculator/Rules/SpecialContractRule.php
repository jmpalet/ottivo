<?php
declare(strict_types=1);

namespace App\Application\Calculator\Rules;

use App\Application\Calculator\Context\CalculationContext;
use App\Domain\Exception\ContractNotSetException;

class SpecialContractRule extends Rule
{
    public function isApplied(CalculationContext $context): bool
    {
        return $this->hasSpecialContract($context);
    }

    public function calculate(CalculationContext $context): int
    {
        if ($context->getEmployee()->getContract() === null) {
            throw new ContractNotSetException('Contract not set');
        }

        return $context->getEmployee()->getContract()->getVacationDays();
    }
}
