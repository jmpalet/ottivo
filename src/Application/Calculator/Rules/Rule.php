<?php
declare(strict_types=1);

namespace App\Application\Calculator\Rules;

use App\Application\Calculator\Context\CalculationContext;

abstract class Rule
{
    abstract public function isApplied(CalculationContext $context): bool;

    abstract public function calculate(CalculationContext $context): int;

    protected function hasSpecialContract(CalculationContext $context): bool {
        return $context->getEmployee()->getContract() !== null;
    }
}
