<?php
declare(strict_types=1);

namespace App\Application\Calculator\Rules;

use App\Application\Calculator\Context\CalculationContext;
use App\Domain\Model\Employee;

class EmployedCurrentYearRule extends Rule
{
    public function isApplied(CalculationContext $context): bool
    {
        return ($context->getYearOfCalculation()->format('Y')
            === $context->getEmployee()->getStartingDate()->format('Y'))
            && !$this->hasSpecialContract($context);
    }

    public function calculate(CalculationContext $context): int
    {
        $month = (int) $context->getEmployee()->getStartingDate()->format('n');
        return (int) ceil(Employee::MIN_VACATION_DAYS/12 * (12 - $month));
    }
}
