<?php
declare(strict_types=1);

namespace App\Application\Calculator\Rules;

use App\Application\Calculator\Context\CalculationContext;
use App\Domain\Model\Employee;

class SeniorityRule extends Rule
{
    private const SENIORITY_YEARS = 30;

    public function isApplied(CalculationContext $context): bool
    {
        return ($context->getEmployee()->getStartingDate()->diff($context->getYearOfCalculation())->y
            >= self::SENIORITY_YEARS) && !$this->hasSpecialContract($context);
    }

    public function calculate(CalculationContext $context): int
    {
        $employedNumOfYears = $context->getEmployee()->getStartingDate()->diff($context->getYearOfCalculation())->y;
        return Employee::MIN_VACATION_DAYS + (int) ceil(($employedNumOfYears - self::SENIORITY_YEARS)/5);
    }
}
