<?php
declare(strict_types=1);

namespace App\Application\Calculator\Command;


use App\Application\Calculator\Context\CalculationContext;
use App\Application\Calculator\Service\VacationCalculator;
use App\Domain\Exception\InvalidStartingDateException;
use App\Domain\Exception\StartingDateAfterCalculationYearException;
use App\Domain\Model\Contract;
use App\Domain\Model\Employee;
use DateTime;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Exception;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Util\Json;

class VacationCommand
{
    /** @var VacationCalculator */
    private $calculator;

    public function __construct()
    {
        $this->calculator = new VacationCalculator();
    }

    public function single(string $startingDate, string $yearOfCalculation, ?string $specialContractVacationDays = null): void
    {
        try {
            // Create model from input values
            $employee = new Employee(new DateTime($startingDate));
            if (null !== $specialContractVacationDays) {
                $employee->setContract(new Contract((int) $specialContractVacationDays));
            }

            // Calculate vacation days and print it in the output
            $vacationDays = $this->calculate($employee, (new DateTime())->setDate((int) $yearOfCalculation, 1, 1));
            print(sprintf('Vacation days: %d' . PHP_EOL, $vacationDays));
        } catch (InvalidStartingDateException|StartingDateAfterCalculationYearException $e) {
            print('ERROR: ' . $e->getMessage() . PHP_EOL);
        } catch (Exception $e) {
            print('ERROR: check the date format. It should be Y-m-d' . PHP_EOL);
        }
    }

    public function multiple(string $fileName, string $yearOfCalculation): void
    {
        AnnotationRegistry::registerLoader('class_exists');
        $serializer = SerializerBuilder::create()->build();

        // Check if input file exists
        if (!file_exists($fileName)) {
            print('ERROR: file does not exist');
            return;
        }

        try {
            // Read from the input file and deserialize to Employee class
            $employees = $serializer->deserialize(file_get_contents($fileName), 'array<App\Domain\Model\Employee>', 'json');

            // Calculate vacation days for each employee and set the value for every object
            foreach ($employees as $employee) {
                /** @var Employee $employee */
                $employee->setVacationDays($this->calculate($employee, (new DateTime())->setDate((int) $yearOfCalculation, 1, 1)));
            }
            // Serialize to JSON
            $jsonData = $serializer->serialize($employees, 'json');

            // Save (prettified) JSON to file
            $fp = fopen('output.json', 'wb');
            fwrite($fp, Json::prettify($jsonData));
            fclose($fp);

            print('✔ Done' . PHP_EOL);
        } catch (InvalidStartingDateException|StartingDateAfterCalculationYearException $e) {
            print('ERROR: ' . $e->getMessage() . PHP_EOL);
        } catch (Exception $e) {
            print('ERROR: check the date format. It should be Y-m-d' . PHP_EOL);
        }
    }

    /**
     * @param Employee $employee
     * @param DateTime $yearOfCalculation
     * @return int
     * @throws Exception
     * @throws StartingDateAfterCalculationYearException
     */
    private function calculate(Employee $employee, DateTime $yearOfCalculation):int {
        $context = new CalculationContext($employee, $yearOfCalculation);
        return $this->calculator->calculate($context);
    }
}
