<?php
declare(strict_types=1);

namespace App\Application\Calculator\Context;

use App\Domain\Model\Employee;
use App\Domain\Exception\StartingDateAfterCalculationYearException;
use DateTime;

class CalculationContext
{
    /** @var Employee */
    private $employee;

    /** @var DateTime */
    private $yearOfCalculation;

    public function __construct(Employee $employee, ?DateTime $yearOfCalculation)
    {
        if ((int) $employee->getStartingDate()->format('Y') > (int) $yearOfCalculation->format('Y')) {
            throw new StartingDateAfterCalculationYearException('Calculation year should be after starting date of the employee');
        }
        $this->employee = $employee;
        $this->yearOfCalculation = $yearOfCalculation;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @return DateTime
     */
    public function getYearOfCalculation(): DateTime
    {
        return $this->yearOfCalculation;
    }

}
