<?php
declare(strict_types=1);

namespace App\Domain\Exception;

use RuntimeException;

class InvalidStartingDateException extends RuntimeException
{
}
