<?php
declare(strict_types=1);

namespace App\Domain\Model;

use JMS\Serializer\Annotation as JMS;

class Contract
{
    /**
     * @var int
     * @JMS\Type("int")
     */
    private $vacationDays;

    public function __construct($vacationDays)
    {
        $this->vacationDays = $vacationDays;
    }

    /**
     * @return int
     */
    public function getVacationDays(): int
    {
        return $this->vacationDays;
    }
}
