<?php
declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Exception\InvalidStartingDateException;
use JMS\Serializer\Annotation as JMS;
use DateTime;

class Employee
{
    /** @var int */
    public const MIN_VACATION_DAYS = 26;

    /**
     * @var DateTime
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $startingDate;

    /**
     * @var Contract
     * @JMS\Type("App\Domain\Model\Contract")
     */
    private $contract;

    /**
     * @var int
     * @JMS\Type("int")
     */
    private $vacationDays;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @var DateTime
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $birthDate;

    /**
     * Employee constructor.
     * @param DateTime $startingDate
     * @throws InvalidStartingDateException
     */
    public function __construct(DateTime $startingDate)
    {
        if (!$this->isStartingDateValid($startingDate)) {
            throw new InvalidStartingDateException('Starting date not valid. Database can only start at the 1st or the 15th of the month.');
        }
        $this->startingDate = $startingDate;
    }

    /**
     * @return DateTime
     */
    public function getStartingDate(): DateTime
    {
        return $this->startingDate;
    }

    /**
     * @return Contract|null
     */
    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    /**
     * @param Contract $contract
     */
    public function setContract(Contract $contract): void
    {
        $this->contract = $contract;
    }

    /**
     * @param int $vacationDays
     */
    public function setVacationDays(int $vacationDays): void
    {
        $this->vacationDays = $vacationDays;
    }

    private function isStartingDateValid(DateTime $startingDate): bool {
        return '01' === $startingDate->format('d') || '15' === $startingDate->format('d');
    }

}
