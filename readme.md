## Description
Script to determine the amount of each employee's vacation days for a given year by running a command line script.
The number of vacation days for an employee is calculated considering the following premises:
* Each employee has a minimum of 26 vacation days regardless of age
* Employee >= 30 years do get one additional vacation day every 5 years
* Employees can have a special contract that overwrites the amount of vacation days
* Employees starting their work at the company in the course of the year get one-twelfth of the their
yearly vacation days starting from the next month's 1st
* Employees can start at the 1st or the 15th of a month

## Requirements
```
composer install
```

## Usage

#### Single employee
Calculate vacation days for a single employee given his/her starting date and the year of calculation.
```
php bin/console single {starting_date:required} {year_of_calculation:required} {special_contract:optional}
```
Arguments:
* `{starting_date}`: Starting date in the format Y-m-d (i.e. 2018-10-01)
* `{year_of_calculation}`: Year of the calculation (i.e. 2019)
* `{special_contract}`: Vacation days of the special contract, if applies (i.e. 30)

Example of command:
```
php bin/console single 2018-10-01 2019 30
```

#### Multiple employees (import from JSON)
Calculate vacation days for a list (array) of employees. The output file will be named as `output.json` and will contain the same JSON structure, with the new field `vacation_days`, for every employee. 
```
php bin/console multiple {file_name:required} {year_of_calculation:required}
```
Arguments:
* `{file_name}`: Path to the JSON file containing the employees database
* `{year_of_calculation}`: Year of the calculation (i.e. 2019)

Example of command:
```
php bin/console multiple test.json 2019
``` 
