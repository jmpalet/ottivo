<?php
declare(strict_types=1);

namespace Tests\Domain;

use Generator;
use \PHPUnit\Framework\TestCase;
use App\Domain\Exception\InvalidStartingDateException;
use App\Domain\Model\Employee;
use DateTime;
use Exception;

class EmployeeTest extends TestCase
{
    /**
     * @dataProvider constructorTestDataProvider
     * @param string $startingDate
     * @param string|null $exception
     * @throws Exception
     */
    public function testConstructor(string $startingDate, string $exception = null): void
    {
        if ($exception !== null) {
            $this->expectException($exception);
        }

        $employee = new Employee(new DateTime($startingDate));

        $this->assertInstanceOf(
            Employee::class,
            $employee
        );
    }

    public function constructorTestDataProvider(): ?Generator
    {
        yield 'Valid constructor -> 1st of Month' => ['2020-01-01'];
        yield 'Valid constructor -> 15th of Month' => ['2020-01-15'];
        yield 'Error in constructor -> invalid date' => ['2020-01-02', InvalidStartingDateException::class];
    }
}
