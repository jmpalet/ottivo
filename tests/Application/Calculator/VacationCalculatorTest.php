<?php
declare(strict_types=1);

namespace Tests\Application\Calculator;

use App\Application\Calculator\Context\CalculationContext;
use App\Application\Calculator\Service\VacationCalculator;
use App\Domain\Model\Contract;
use App\Domain\Model\Employee;
use DateTime;
use Exception;
use Generator;
use PHPUnit\Framework\TestCase;

class VacationCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculatorTestDataProvider
     * @param Employee $employee
     * @param Contract $contract
     * @param int $yearOfCalculation
     * @param $expectedVacationDays
     * @throws Exception
     */
    public function testCalculator(Employee $employee, ?Contract $contract, int $yearOfCalculation, int $expectedVacationDays): void
    {
        if ($contract instanceof Contract) {
            $employee->setContract($contract);
        }

        $calculator = new VacationCalculator();
        $this->assertEquals($expectedVacationDays, $calculator->calculate(new CalculationContext($employee, (new DateTime())->setDate($yearOfCalculation, 1, 1))));
    }

    public function calculatorTestDataProvider(): ?Generator
    {
        yield 'Basic holidays, no rule applied' => [new Employee(new DateTime('2016-02-01')), null, 2019, Employee::MIN_VACATION_DAYS];
        yield 'New employee started on mid March' => [new Employee(new DateTime('2019-03-15')), null, 2019, 20];
        yield 'New employee started on first day of the year' => [new Employee(new DateTime('2019-01-01')), null, 2019, 24];
        yield 'Employed since 43 years (30+5+5+4)' => [new Employee(new DateTime('2000-06-1')), null, 2044, Employee::MIN_VACATION_DAYS+3];
        yield 'Employed since 31 years (30+1)' => [new Employee(new DateTime('2000-06-1')), null, 2032, Employee::MIN_VACATION_DAYS+1];
        yield 'Employed since 56 years (30+(5*5)+1)' => [new Employee(new DateTime('2000-06-1')), null, 2057, Employee::MIN_VACATION_DAYS+6];
    }
}
